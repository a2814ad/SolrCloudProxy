FROM openjdk:alpine
RUN mkdir /solrcloudproxy
RUN apk add --update bash
COPY SolrCloudProxy-FullBundle-Latest.jar /solrcloudproxy
COPY contrib/startup.sh /solrcloudproxy
WORKDIR /solrcloudproxy
EXPOSE 8983/tcp
CMD ["./startup.sh"]

