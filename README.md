## SolrCloudProxy

SolrCloudProxy can be used by solr clients, that are not solr cloud aware. 
SolrCloudProxy connects to the zookeeper servers and solr workers using SolrJ. 
The proxy then accepts HTTP connections and sends them to the solr cloud. Cloud
events like node/zookeeper failures are handled by SolrJ. The intended use is, 
that every solr client uses a locally installed SolrCloudProxy to connect to the
cloud.

## Security Filter

SolrCloudProxy can also be used to filter Solr queries using regular expressions
containing IP Adress filters, URI filters and parameter filters. See SecurityFilterExample.xml
for a example.

## Installation

run install.sh in the contrib directory. If maven is set up, installer builds
the jar and installs it in /opt/SolrCloudProxy. It also creates a systemd service.

## Status

Not tested, not feature complete! Not all ResponseWriters work, some probably
will never work (velocity, xslt)!