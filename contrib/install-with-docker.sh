#!/bin/bash

INSTALLUSER=solr
INSTALLBASEPATH=/home/$INSTALLUSER
INSTALLFULLPATH="" # to be determined later on
MINIMUMPORT=3000

function IsInteger()
{
    [[ ${1} == ?(-)+([0-9]) ]]
}

function TestForRequirementsOrExit()
{
  if [ $(uname -s) != "Linux" ]
  then
    echo "Error: This is not a Linx machine."
    exit 1
  fi

  if [ $(id -u) -ne 0 ]
  then
    echo "Error: Root privileges not provided."
    exit 1
  fi

  if ! $(pidof systemd > /dev/null)
  then
    echo "Error: Systemd not provided."
    exit 1
  fi

  if [ "$(which curl)" == "" ]
  then
    echo "Error: curl not provided. Install with 'apt install curl -y'."
    exit 1
  fi

  if [ "$(which docker)" == "" ]
  then
    echo "Error: Dockerd not provided. Setup docker using 'curl -fsSL get.docker.com | sh'."
    exit 1
  fi

  if ! getent passwd $INSTALLUSER > /dev/null
  then
    echo "Error: A suiting unix user $INSTALLUSER does not exist. Setup with 'useradd -g docker -d $INSTALLBASEPATH -s /bin/bash -m $INSTALLUSER'."
    exit 1
  fi

  if ! IsInteger $1 || [ $1 -lt $MINIMUMPORT ]
  then
    echo "Error: Specify a valid port number for this service >= 3000, e.g. $0 3000"
    exit 1
  fi

  INSTALLFULLPATH=$INSTALLBASEPATH/SolrCloudProxy/$1

  if [ -d $INSTALLFULLPATH ]
  then
    echo "Error: Appears that a service was already registered for port $1, see $INSTALLFULLPATH"
    exit 1
  fi
}

function PlaceBaseConfigFiles()
{
  mkdir -p $INSTALLFULLPATH/conf
cat <<EOF > $INSTALLFULLPATH/conf/SolrCloudProxy.properties
ListenHost=0.0.0.0
ListenPort=8983
SecurityFilterFile=conf/SecurityFilter.xml
log4j.configurationFile=conf/log4j2.xml
EOF
  echo "$INSTALLFULLPATH/conf/SolrCloudProxy.properties was created, please configure."

  curl -fsSL https://gitlab.lrz.de/a2814ad/SolrCloudProxy/raw/master/SecurityFilterExample.xml -o $INSTALLFULLPATH/conf/SecurityFilter.xml
  echo "$INSTALLFULLPATH/conf/SecurityFilter.xml was created, please configure."

  curl -fsSL https://gitlab.lrz.de/a2814ad/SolrCloudProxy/raw/master/log4j2.xml.example -o $INSTALLFULLPATH/conf/log4j2.xml
  echo "$INSTALLFULLPATH/conf/log4j2.xml was created, please configure."

  chown -R $INSTALLUSER:docker $INSTALLBASEPATH
}


function SetupSystemdService()
{
cat <<EOF > /etc/systemd/system/solrcloudproxy-$1.service
[Unit]
Description=SolrCloudProxy docker instance on port $1
After=nss-lookup.target network.target docker.service
Requires=docker.service

[Service]
Type=simple
User=solr
WorkingDirectory=/home/solr/SolrCloudProxy/$1
ExecStartPre=-/usr/bin/docker rm solrcloudproxy-$1
ExecStart=/usr/bin/docker run --name solrcloudproxy-$1 -p $1:8983 -v $INSTALLFULLPATH/conf:/solrcloudproxy/conf gitlab.lrz.de:5005/a2814ad/solrcloudproxy:latest
ExecStop=/usr/bin/docker kill solrcloudproxy-$1
SyslogIdentifier=SolrCloudProxy-docker-$1
Restart=always

[Install]
WantedBy=multi-user.target
DefaultInstance=default
EOF

  systemctl daemon-reload
  echo "Start service using: "
  echo "systemctl enable solrcloudproxy-3000"
  echo "systemctl start solrcloudproxy-3000"
}


# "main"
TestForRequirementsOrExit $1
PlaceBaseConfigFiles $1
SetupSystemdService $1

exit 0
