#!/bin/bash

INSTALLPATH=/opt/SolrCloudProxy
INSTALLUSER=solr

if [ `uname -s` != "Linux" ] ; then 
	echo "Installer only works for Linux"
	exit 1
fi

if ! `pidof systemd > /dev/null` ; then
	echo "Installer only works with systemd"
	exit 1
fi

if ! getent passwd $INSTALLUSER > /dev/null ; then
	echo "user $INSTALLUSER not present. Please create a unix user $INSTALLUSER before retrying installer."
	exit 1
fi

if [ `id -u` -ne 0 ] ; then
	echo "You have to be root to install SolrCloudProxy"
	exit 1
fi

mkdir -p ${INSTALLPATH}/conf
mkdir -p ${INSTALLPATH}/log


if [ ! -f ../target/SolrCloudProxy-*-jar-with-dependencies.jar ] ; then
	echo "trying to download jar file"
	curl -Ls https://gitlab.lrz.de/a2814ad/SolrCloudProxy/-/jobs/artifacts/master/download?job=build_solrcloudproxy -o ../target/download.zip
	(cd ../target && unzip download.zip 2>&1 > /dev/null )
fi
if [ ! -f ../target/SolrCloudProxy-*-jar-with-dependencies.jar ] ; then
	echo "trying to build jar file"
	(cd .. && maven package)
fi
if [ ! -f ../target/SolrCloudProxy-*-jar-with-dependencies.jar ] ; then
	echo "failed!"
	exit 1
fi

cp ../target/SolrCloudProxy-*-jar-with-dependencies.jar $INSTALLPATH/
ln -s $INSTALLPATH/SolrCloudProxy-*-jar-with-dependencies.jar $INSTALLPATH/SolrCloudProxy.jar
if [ ! -e $INSTALLPATH/conf/SolrCloudProxy-default.properties ] ; then
	cp ../SolrCloudProxy.properties.example $INSTALLPATH/conf/SolrCloudProxy-default.properties
fi
if [ ! -e $INSTALLPATH/conf/log4j2-default.xml ] ; then
	cp ../log4j2.xml.example $INSTALLPATH/conf/log4j2-default.xml
fi
touch $INSTALLPATH/conf/SolrCloudProxy-default.env

chown -R $INSTALLUSER ${INSTALLPATH}
cp solrcloudproxy@.service /usr/lib/systemd/system/
systemctl daemon-reload

echo "Service installed. Configure your SolrCloudProxy in $INSTALLPATH/conf/SolrCloudProxy-default.properties"
echo "Start service using: "
echo "systemctl enable solrcloudproxy@default"
echo "systemctl start solrcloudproxy@default"
echo
echo "If you want to run multiple instances, you have to create another config in:"
echo "$INSTALLPATH/conf/SolrCloudProxy-newinstance.properties"
echo "and enable/start it with:"
echo "systemctl enable solrcloudproxy@newinstance"
echo "systemctl start solrcloudproxy@newinstance"

