#!/bin/bash

if [ -d tempConf ]
then
  rm -rf tempConf
fi
mkdir tempConf

cat <<EOF > tempConf/SolrCloudProxy.properties
ListenHost=0.0.0.0
ListenPort=8983
SecurityFilterFile=conf/SecurityFilter.xml
EOF

cat <<EOF > tempConf/SecurityFilter.xml
<?xml version="1.0" encoding="UTF-8"?>
<filter name="Example Filter" defaultResult="true">
        <rule name="Block one IP">
                <deniedIPRegex>10\.10\.10\.10</deniedIPRegex>
        </rule>
        <rule name="allow select handler for a IP range, disallowing parameter df">
                <allowedIPRegex>10\.20\.30\..*</allowedIPRegex>
                <allowedURIRegex>.*/select</allowedURIRegex>
                <deniedParamRegex param="df">.*</deniedParamRegex>
        </rule>
</filter>
EOF
