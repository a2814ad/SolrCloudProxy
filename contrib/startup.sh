#!/bin/bash

if [ ! -f conf/SolrCloudProxy.properties ]
then
  echo "Please provide conf/SolrCloudProxy.properties file."
  exit -1
fi

java -DpropertiesFile=conf/SolrCloudProxy.properties -jar SolrCloudProxy-FullBundle-Latest.jar
