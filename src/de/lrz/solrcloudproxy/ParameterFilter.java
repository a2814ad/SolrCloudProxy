package de.lrz.solrcloudproxy;

// source: https://leonardom.wordpress.com/2009/08/06/getting-parameters-from-httpexchange/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;

public class ParameterFilter extends Filter {
	
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	   @Override
	    public String description() {
	        return "Parses the requested URI for parameters";
	    }

	    @Override
	    public void doFilter(HttpExchange exchange, Chain chain)
	        throws IOException {
	    	log.debug("Filtering...");
	        parseGetParameters(exchange);
	        parsePostParameters(exchange);
	        chain.doFilter(exchange);
	    }    

	    private void parseGetParameters(HttpExchange exchange)
	        throws UnsupportedEncodingException {
	    	log.debug("Parsing GETs...");
	        Map<String, Object> parameters = new HashMap<String, Object>();
	        URI requestedUri = exchange.getRequestURI();
	        String query = requestedUri.getRawQuery();
	        parseQuery(query, parameters);
	        exchange.setAttribute("parameters", parameters);
	    }

	    private void parsePostParameters(HttpExchange exchange)
	        throws IOException {

	        if ("post".equalsIgnoreCase(exchange.getRequestMethod())) {
	            @SuppressWarnings("unchecked")
	            Map<String, Object> parameters =
	                (Map<String, Object>)exchange.getAttribute("parameters");
	            InputStreamReader isr =
	                new InputStreamReader(exchange.getRequestBody(),"utf-8");
	            BufferedReader br = new BufferedReader(isr);
	            String query = br.readLine();
	            parseQuery(query, parameters);
	        }
	    }

	     @SuppressWarnings("unchecked")
	     private void parseQuery(String query, Map<String, Object> parameters)
	         throws UnsupportedEncodingException {
	    	 log.debug("parseQuery...");
	         if (query != null) {
	             String pairs[] = query.split("[&]");

	             for (String pair : pairs) {
	                 String param[] = pair.split("[=]");

	                 String key = null;
	                 String value = null;
	                 if (param.length > 0) {
	                     key = URLDecoder.decode(param[0],
	                         System.getProperty("file.encoding"));
	                 }

	                 if (param.length > 1) {
	                     value = URLDecoder.decode(param[1],
	                         System.getProperty("file.encoding"));
	                 }

	                 if (parameters.containsKey(key)) {
	                     Object obj = parameters.get(key);
	                     if(obj instanceof List<?>) {
	                         List<String> values = (List<String>)obj;
	                         values.add(value);
	                     } else if(obj instanceof String) {
	                         List<String> values = new ArrayList<String>();
	                         values.add((String)obj);
	                         values.add(value);
	                         parameters.put(key, values);
	                     }
	                 } else {
	                	 log.debug("Found HTTP Parameter "+key);
	                     parameters.put(key, value);
	                 }
	             }
	         }
	    }

}
