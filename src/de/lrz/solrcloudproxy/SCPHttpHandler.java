package de.lrz.solrcloudproxy;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class SCPHttpHandler implements HttpHandler {

	private String zkHostString = null;
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	public void handle(HttpExchange httpExchange) throws IOException {
		// System.out.println("HttpHandler.handle("+httpExchange.getRequestURI().toString()
		// +")");
		zkHostString = SolrCloudProxy.getStringProperty("ZookeeperString");
		// Build a SolrQuery from HTTP request variables
		Map<String, Object> params = (Map<String, Object>) httpExchange.getAttribute("parameters");
		Future<String> future = null;
		
		// check Security filter
		if (SolrCloudProxy.getSecurityFilter().checkRules(httpExchange.getRequestURI(), params,
				httpExchange.getRemoteAddress()) == false) {
			log.info("Access denied by security filter");
			sendErrorDocument(httpExchange, 403, "Access denied by security filter");
			return;
		} else {
			// OK, send query to solr.
			log.info("starting Solr task");
			try {
				future = SolrCloudProxy.getSolrThreadPool().submit(new SolrClientTask(zkHostString,
						httpExchange.getRequestURI(), params, httpExchange.getRemoteAddress()));
			} catch (RejectedExecutionException re) {
				log.error("Thread cannot run: {}", re.toString());
				sendErrorDocument(httpExchange, 500, SCPHttpHandler.class + ": " + re.toString());
				return;
			}
		}


		try {
			OutputStream os = httpExchange.getResponseBody();
			String outstring = null;
			log.info("Fetching Solr response");
			outstring = future.get();
			httpExchange.getResponseHeaders().set("Content-Type", getMimetype(params));
			httpExchange.sendResponseHeaders(200, outstring.getBytes().length);
			os.write(outstring.getBytes());
			log.info("Sending HTTP response: {} Bytes",outstring.getBytes().length);
			os.close();
		} catch (InterruptedException ie) {
			String errstring = SCPHttpHandler.class + ": " + ie.toString();
			sendErrorDocument(httpExchange, 500, errstring);
			log.warn("InterruptedException in {}: {}", SCPHttpHandler.class, ie.fillInStackTrace());
		} catch (ExecutionException ex) {
			String errstring = SCPHttpHandler.class + ": " + ex.toString();
			sendErrorDocument(httpExchange, 500, errstring);
			log.warn("ExecutionException in {}: {}", SCPHttpHandler.class, ex.fillInStackTrace());
		} catch (Exception ee) {
			String errstring = SCPHttpHandler.class + ": " + ee.toString();
			sendErrorDocument(httpExchange, 500, errstring);
			log.warn("Exception in {}: {}", SCPHttpHandler.class, ee.fillInStackTrace());
		}
		
		
	}

	private void sendErrorDocument(HttpExchange httpExchange, int errNo, String message) {
		log.debug("Sending http Error Document");
		httpExchange.getResponseHeaders().set("Content-Type", "text/html; charset=UTF-8");
		try {
			
			message = "<html><head></head><body><h1>Error " + errNo + "</h1><br /><pre>" + message
					+ "</pre></body></html>";
			httpExchange.sendResponseHeaders(errNo, message.getBytes().length);
			OutputStream os = httpExchange.getResponseBody();
			os.write(message.getBytes());
		    os.close();
		} catch (IOException e) {
			log.warn("Exception in " + SCPHttpHandler.class, e.fillInStackTrace());
		}
	}

	private String getMimetype(Map<String, Object> params) {
		String mimetype = "application/xml; charset=UTF-8";

		if (params.containsKey("wt")) {
			String responseWriter = (String) params.get("wt");
			log.debug("selecting mimetype because of wt=" + responseWriter);
			if (responseWriter.equalsIgnoreCase("ruby") || responseWriter.equalsIgnoreCase("json")
					|| responseWriter.equalsIgnoreCase("csv") || responseWriter.equalsIgnoreCase("python")) {
				mimetype = "text/plain;charset=utf-8";
			}
			if (responseWriter.equalsIgnoreCase("php")) {
				mimetype = "text/x-php;charset=UTF-8";
			}
			if (responseWriter.equalsIgnoreCase("phps")) {
				mimetype = "text/x-php-serialized;charset=UTF-8";
			}
			// TODO: check: probably only error messages have html/iso-8859?
			if (responseWriter.equals("xslt") || responseWriter.equals("geojson")) {
				mimetype = "text/html;charset=iso-8859-1";
			}

			if (responseWriter.equals("smile") || responseWriter.equals("javabin")) {
				mimetype = "application/octet-stream";
			}

		}

		return mimetype;
	}

}
