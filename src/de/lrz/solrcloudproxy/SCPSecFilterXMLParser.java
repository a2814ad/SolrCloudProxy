package de.lrz.solrcloudproxy;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;
import static javax.xml.stream.XMLStreamReader.*;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;

import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author a2814ad
 * 
 * Generates a SCPSecurityFilter Object from a XML File.
 */
public class SCPSecFilterXMLParser {
	
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	public static SCPSecurityFilter parseFile(String filename) throws XMLStreamException {
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLStreamReader stax = inputFactory.createXMLStreamReader(new StreamSource(filename));
		return parseXMLStream(stax);
	}
	
	public static SCPSecurityFilter parseXMLStream(XMLStreamReader stax) throws XMLStreamException {
		SCPSecurityFilter securityFilter=new SCPSecurityFilter();
		
		ArrayList<String> state=new ArrayList<String>(); // saves the state
		while(stax.hasNext()) {
			int eventType = stax.nextTag();
			String elementName = stax.getName().toString();
			
			switch(eventType) {
			case START_ELEMENT:
				if (elementName.equals("filter")) {
					state.add("filter");	
					for(int i=0; i< stax.getAttributeCount(); i++) {
						if(stax.getAttributeLocalName(i).equals("name")) {
							log.info("Setting security filter name to '{}'",stax.getAttributeValue(i));
							securityFilter.setFilterName(stax.getAttributeValue(i));
						}
						if(stax.getAttributeLocalName(i).equals("defaultResult")) {
							if(stax.getAttributeValue(i).equals("true")) {
								securityFilter.setDefaultResult(true);
							}
							else if(stax.getAttributeValue(i).equals("false")) {
								securityFilter.setDefaultResult(false);
							}
							else throw new XMLStreamException("Wrong attribute value in attribute defaultResult");
						}
					}			
				}
				else if (elementName.equals("rule")) {
					// todo: check if we have the correct state?
					state.add("rule");
					log.info("Creating new rule");
					SCPSecurityRule rule = new SCPSecurityRule();
					for(int i=0; i< stax.getAttributeCount(); i++) {
						if(stax.getAttributeLocalName(i).equals("name")) {
							log.info("Setting rule name to '{}'",stax.getAttributeValue(i));
							rule.setRuleName(stax.getAttributeValue(i));
						}
						
					}
					securityFilter.add(rule);
				}
				else if (elementName.equals("deniedIPRegex") || elementName.equals("allowedIPRegex") || elementName.equals("deniedURIRegex") || elementName.equals("allowedURIRegex")) {
					state.add(elementName);
					log.info("Setting a {}", elementName);
					SCPSecurityRule rule=securityFilter.get(securityFilter.size()-1);
					if(elementName.equals("deniedIPRegex")) {
						rule.setDeniedIPAddressRegex(stax.getElementText());
					} else if (elementName.equals("allowedIPRegex")){
						rule.setAllowedIPAddressRegex(stax.getElementText());
					} else if (elementName.equals("allowedURIRegex")){
						rule.setAllowedURIRegex(stax.getElementText());
					} else if (elementName.equals("deniedURIRegex")){
						rule.setDeniedURIRegex(stax.getElementText());
					}
				}
				else if (elementName.equals("deniedParamRegex") || elementName.equals("allowedParamRegex") ) {
					state.add(elementName);
					log.info("Adding a {}", elementName);
					String param="";
					for(int i=0; i< stax.getAttributeCount(); i++) {
						if(stax.getAttributeLocalName(i).equals("param")) {
							param=stax.getAttributeValue(i);
						}
					}
					
					SCPSecurityRule rule=securityFilter.get(securityFilter.size()-1);
					if(elementName.equals("deniedParamRegex")) {
						rule.addDeniedParameterRegex(param, stax.getElementText());
					} else {
						rule.addAllowedParameterRegex(param, stax.getElementText());
					}
				}
				break;
			case END_ELEMENT:
				if(elementName.equals("filter")) {
					return securityFilter;
				}
				break;
			case END_DOCUMENT:
				return securityFilter;

			}

		}
		
		
		return securityFilter;
	}
}
