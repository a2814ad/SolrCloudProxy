package de.lrz.solrcloudproxy;

import java.lang.invoke.MethodHandles;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;

/**
 * @author a2814ad A SCPSecurityFilter contains 0 or more SCPSecurityRules. Only
 *         one filter is allowed at the moment
 */
public class SCPSecurityFilter extends ArrayList<SCPSecurityRule> {

	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	@Getter
	@Setter
	private String filterName = "default";
	@Getter
	@Setter
	private boolean defaultResult = false;

	public boolean checkRules(URI requestURI, Map<String, Object> params, InetSocketAddress remoteIP) {
		String ip=remoteIP.getAddress().toString().substring(1);
		log.debug("checkRules({}, params, {})",requestURI.toString(),ip);
		for (SCPSecurityRule rule : this) {
			Boolean retval = null;
			log.info("Checking IP Address {} against rule '{}'",ip, rule.getRuleName());
			retval = rule.checkIPAddress(ip);
			if(retval != null) return retval;
			
			log.info("Checking URI {} against rule '{}'", requestURI.toString(),rule.getRuleName());
			retval = rule.checkURI(requestURI.toString());
			if(retval != null) return retval;
			
			for (Map.Entry<String, Object> param : params.entrySet()) {
				log.info("Checking parameter {} against rule '{}'" ,param.getKey(), rule.getRuleName());
				retval = rule.checkParameter(param.getKey(), param.getValue());
				if(retval != null) return retval;
			}

		}
		return defaultResult;
	}

}
