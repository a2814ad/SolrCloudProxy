package de.lrz.solrcloudproxy;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;

/**
 * @author a2814ad
 * SCPSecurityRule can be used to filter Solr Queries.
 */
public class SCPSecurityRule {
	
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@Getter @Setter private String ruleName="NoName";
	
	@Getter private String allowedIPAddressRegex=null;
	private Pattern allowedIPAddressPattern;
	@Getter private String deniedIPAddressRegex=null;
	private Pattern deniedIPAddressPattern;	
	@Getter private String allowedURIRegex=null;
	private Pattern allowedURIPattern;
	@Getter private String deniedURIRegex=null;
	private Pattern deniedURIPattern;
	private Map<String,String> allowedParameterRegex = new HashMap<String,String>();
	private Map<Pattern,Pattern> allowedParameterPattern = new HashMap<Pattern,Pattern>();
	private Map<String,String> deniedParameterRegex = new HashMap<String,String>();
	private Map<Pattern,Pattern> deniedParameterPattern = new HashMap<Pattern,Pattern>();

	public void setAllowedIPAddressRegex(String regex) {
		allowedIPAddressRegex = regex;
		allowedIPAddressPattern = Pattern.compile(regex);
	}

	public void setDeniedIPAddressRegex(String regex) {
		deniedIPAddressRegex = regex;
		deniedIPAddressPattern = Pattern.compile(regex);
	}
	
	public void setAllowedURIRegex(String regex) {
		allowedURIRegex = regex;
		allowedURIPattern = Pattern.compile(regex);
	}

	public void setDeniedURIRegex(String regex) {
		deniedURIRegex = regex;
		deniedURIPattern = Pattern.compile(regex);
	}

	public void addAllowedParameterRegex(String parameter, String value) {
		allowedParameterRegex.put(parameter, value);
		allowedParameterPattern.put(Pattern.compile(parameter), Pattern.compile(value));
	}
	
	public void addDeniedParameterRegex(String parameter, String value) {
		deniedParameterRegex.put(parameter, value);
		deniedParameterPattern.put(Pattern.compile(parameter), Pattern.compile(value));
	}
		
	/**
	 * @param ip
	 * @return
	 * 
	 * Checks the string representation of a IP address against regular expressions.
	 * First checks against allowedIPAddressRegex if this is not empty. Returns true, if it matches.
	 * Then checks against deniedIPAddressRegex if this is not empty. Returns false, if it matches.
	 * If there are no matches, returns null. 
	 * 
	 */
	public Boolean checkIPAddress(String ip) {
		if(allowedIPAddressRegex!=null && !allowedIPAddressRegex.isEmpty() && allowedIPAddressPattern.matcher(ip).matches()) {
			log.debug("checkIPAddress: true");
			return true;
		}
		if(deniedIPAddressRegex!=null && !deniedIPAddressRegex.isEmpty() && deniedIPAddressPattern.matcher(ip).matches()) {
			log.debug("checkIPAddress: false");
			return false;
		}
		log.debug("checkIPAddress: null");
		return null;
	}
	
	/**
	 * @param parameter
	 * @param value
	 * @return
	 * 
	 * Checks the solr query parameters against regular expressions.
	 * First checks against all allowedParameterRegex if this is not empty. Returns true, if one pair matches.
	 * Then checks against deniedParameterRegex if this is not empty. Returns false, if one pair matches.
	 * If there are no matches, returns null. 
	 * 
	 */
	public Boolean checkParameter(String parameter, Object value) {
		
		ArrayList<String> values = new ArrayList<String>();
		if(value instanceof String) {
			values.add((String) value);
		} else if (value instanceof ArrayList) {
			try {
				for (Object v : (ArrayList<Object>) value) {
					if(v instanceof String) {
						values.add((String) v);
					}
				}
			} catch (Exception e) {
				log.error("Unknown ArrayList Object");
			}
		} else {
			log.error("Unknown parameter value for parameter {}", parameter);
		}
		
		Iterator<Map.Entry<Pattern,Pattern>> it = allowedParameterPattern.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<Pattern,Pattern> pair = it.next();
			for (String v: values) {
				if(pair.getKey().matcher(parameter).matches() && pair.getValue().matcher(v).matches()) {
					return true;
				}
			}
		}		
		
		it = deniedParameterPattern.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<Pattern,Pattern> pair = it.next();
			for (String v: values) {
				if(pair.getKey().matcher(parameter).matches() && pair.getValue().matcher(v).matches()) {
					return false;
				}
			}
		}

		return null;
	}
	
	
	/**
	 * @param ip
	 * @return
	 * 
	 * Checks the string representation of a URI against regular expressions.
	 * First checks against allowedURIRegex if this is not empty. Returns true, if it matches.
	 * Then checks against deniedURIRegex if this is not empty. Returns false, if it matches.
	 * If there are no matches, returns null. 
	 * 
	 */
	public Boolean checkURI(String uri) {
		if(allowedURIRegex!=null && !allowedURIRegex.isEmpty() && allowedURIPattern.matcher(uri).matches()) {
			return true;
		}
		if(deniedURIRegex !=null && !deniedURIRegex.isEmpty() && deniedURIPattern.matcher(uri).matches()) {
			return false;
		}
		return null;
	}	
	

}
