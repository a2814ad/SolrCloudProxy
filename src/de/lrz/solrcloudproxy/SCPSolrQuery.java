package de.lrz.solrcloudproxy;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SCPSolrQuery extends SolrQuery {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	public SCPSolrQuery(Map<String, Object> requestParams) {
		super();
		for (Map.Entry<String, Object> item : requestParams.entrySet()) {
			if (item.getValue() instanceof String) {
				setParam(item.getKey(), (String) item.getValue());
			} else if (item.getValue() instanceof ArrayList) {
				// TODO: Check, if there is a easier method to safely convert from ArrayList<Object> to String[] 
				try {
					ArrayList<String> al=(ArrayList<String>) item.getValue();
					Object[] values = al.toArray();
					String[] svalues= new String[values.length];
					for(int i=0; i<values.length; i++) {
					
						if(values[i] instanceof String) {
							svalues[i]=(String) values[i];
						} else {
							svalues[i]="";
						}
					}
					set(item.getKey(),svalues);
				} catch (Exception e) {
					log.warn("Parameter {} Error converting to String[]: {}", item.getKey(), e.fillInStackTrace());
				}
			} else {
				log.warn("Parameter is not String or ArrayList: {}", item.getKey());
			}
		}
	}

}
