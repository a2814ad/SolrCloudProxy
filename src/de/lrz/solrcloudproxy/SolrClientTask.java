package de.lrz.solrcloudproxy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.invoke.MethodHandles;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.SimpleOrderedMap;
import org.apache.solr.request.LocalSolrQueryRequest;
import org.apache.solr.response.BinaryQueryResponseWriter;
import org.apache.solr.response.CSVResponseWriter;
import org.apache.solr.response.GeoJSONResponseWriter;
import org.apache.solr.response.JSONResponseWriter;
import org.apache.solr.response.PHPResponseWriter;
import org.apache.solr.response.PHPSerializedResponseWriter;
import org.apache.solr.response.PythonResponseWriter;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.response.XMLResponseWriter;
import org.apache.solr.response.QueryResponseWriter;
import org.apache.solr.response.RubyResponseWriter;
import org.apache.solr.response.SmileResponseWriter;
import org.apache.solr.response.XSLTResponseWriter;
import org.apache.solr.response.BinaryResponseWriter;
import org.slf4j.LoggerFactory;

// idea: http://stackoverflow.com/questions/16680096/pre-initializing-a-pool-of-worker-threads-to-reuse-connection-objects-sockets
public class SolrClientTask implements Callable<String> {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private static String zookeeperString=null;
	private URI requestURI;
	private InetSocketAddress remoteIP;
	private Map<String, Object> params;
	private static long counter=0;
	private static long elapsedTimeCumulated=0;
	
	private static final ThreadLocal<CloudSolrClient> threadLocal = new ThreadLocal<CloudSolrClient>() {
		@Override
		protected CloudSolrClient initialValue() {
			log.info("Initializing SolrClient");
			return new CloudSolrClient.Builder().
					withZkHost(zookeeperString).build();
		}
	};

	
	/**
	 * @param zkString: Zookeeper String
	 * @param requestURI: Request URI
	 * @param params: Request Parameters
	 */
	public SolrClientTask(String zkString,URI requestURI,Map<String, Object> params, InetSocketAddress remoteIP ) {
		zookeeperString = zkString;
		this.requestURI = requestURI;
		this.params = params;
		this.remoteIP = remoteIP;
	}

	@Override
	public String call() {
		long starttime=System.nanoTime();
		counter++;
		log.info("Starting Task #{}",counter);
		
		log.debug("Checking security filter");
		if(SolrCloudProxy.getSecurityFilter().checkRules(requestURI, params, remoteIP) == false) {
			// Todo: create a access denied answer
		}

		
		CloudSolrClient solrClient=threadLocal.get();
		SCPSolrQuery q=new SCPSolrQuery(params);

		String[] path=requestURI.getPath().split("/");
		String method="";
		String collectionName ="";
		if(path.length >1) {
			method=path[path.length-1];
			collectionName = path[path.length-2];
		}
		
		if(!method.equals("select")) {
			log.info("Collection Name: {}",collectionName);
			log.info("Method: {}",method);
			q.setRequestHandler("/"+method);
		}
		
		QueryResponse queryResponse=null;
		
		
		
		log.debug("SolrQuery: {}", q.toString());
		try {
			queryResponse=solrClient.query(collectionName,q);
			log.info("Got Solr response");
		} catch (SolrServerException e) {
			log.error("Solr Exception: {}",e.toString());
			// TODO: return error 
		} catch (IOException e) {
			log.error("IO Exception: {}",e.toString());
			// TODO: return error 
		}
		
		String outstring="";
		
		if(params.containsKey("wt")) {
			String responseWriter=(String) params.get("wt");
			log.info("selecting response writer because of wt={}",responseWriter);
			outstring=generateResponse(responseWriter.toLowerCase(),q, queryResponse);
		} 
		
		if(outstring.isEmpty()) {
			// default XML output
			outstring=generateResponse("xml",q, queryResponse);
		}
		
		
		log.info("Returning Solr response");
		long endtime=System.nanoTime();
		elapsedTimeCumulated+=endtime-starttime;
		log.info("Statistics: SolrQuery Time: {} Average: {}",(endtime-starttime)/10E9,(elapsedTimeCumulated/counter/10E9));
		log.info("Statistics: {} queries", counter);
		log.debug("Returned: {}", outstring);
		return outstring;
		
	}
	
	private void fixResponseHeaders(QueryResponse response, String format) {
		log.debug("Fixing QueryResponse Headers");
		SimpleOrderedMap<String> p= (SimpleOrderedMap<String>) response.getHeader().get("params");
		p.remove("wt");
		p.remove("version");
		p.remove("_stateVer_");
		p.add("wt", format);
		response.getHeader().remove("params");
		response.getHeader().add("params", p);
	}
	
	// http://stackoverflow.com/questions/16029483/solrj-client-and-xml-response
	private String generateResponse(String format,SolrParams request, QueryResponse response) {
		log.info("Generating {} Response", format);
		
		fixResponseHeaders(response, format);
		
		if(format.equals("smile") || format.equals("javabin")) {
			return generateBinaryResponse(format, request, response);
		}
		
		QueryResponseWriter responseWriter = null;
		Writer w = new StringWriter();
		
		if(format.equals("geojson")) {
			responseWriter = new GeoJSONResponseWriter();
		}
		if(format.equals("json")) {
			responseWriter = new JSONResponseWriter();
		}
		if(format.equals("php")) {
			responseWriter = new PHPResponseWriter();
		}
		if(format.equals("phps")) {
			responseWriter = new PHPSerializedResponseWriter();
		}
		if(format.equals("ruby")) {
			responseWriter = new RubyResponseWriter();
		}
		if(format.equals("csv")) {
			responseWriter = new CSVResponseWriter();
		}
		if(format.equals("python")) {
			responseWriter = new PythonResponseWriter();
		}		
		if(format.equals("xslt")) {
			responseWriter = new XSLTResponseWriter();
		}
		if(responseWriter== null || format.equals("xml")) {
			responseWriter = new XMLResponseWriter();
		}
	    
	    SolrQueryResponse sResponse = new SolrQueryResponse();
	    sResponse.setAllValues(response.getResponse());
	    try {
	    	responseWriter.write(w, new LocalSolrQueryRequest(null, request), sResponse);
	    } catch (IOException e) {
	        throw new RuntimeException("Unable to convert Solr response into "+format, e);
	    }
	    return w.toString();
	}
	
	
	private String generateBinaryResponse(String format,SolrParams request, QueryResponse response) {
		BinaryQueryResponseWriter responseWriter = null;
		OutputStream w = new ByteArrayOutputStream();
		if(format.equals("smile")) {
			responseWriter = new SmileResponseWriter();
		}
		// or can we dump the response directly?
		if(format.equals("javabin")) {
			responseWriter = new BinaryResponseWriter();
		}
		SolrQueryResponse sResponse = new SolrQueryResponse();
	    sResponse.setAllValues(response.getResponse());
	    try {
	        responseWriter.write(w, new LocalSolrQueryRequest(null, request), sResponse);
	    } catch (IOException e) {
	        throw new RuntimeException("Unable to convert Solr response into "+format, e);
	    }
	    return w.toString();
	}


}
