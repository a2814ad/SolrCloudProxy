package de.lrz.solrcloudproxy;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.InetSocketAddress;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import lombok.Getter;

import javax.xml.stream.XMLStreamException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.log4j.PropertyConfigurator;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;

public class SolrCloudProxy {

	private static Properties properties=null;
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	private final static String[][] defaultProperties = {
			{"ZookeeperString", "solr1:2181,solr2:2181,solr3:2181"},
			{"ListenHost","localhost"},
			{"ListenPort","8983"},
			{"solrThreadsCorePoolSize","2"},
			{"solrThreadsMaxPoolSize","20"},
			{"solrThreadsKeepAliveMins","30"},
			{"log4j.rootLogger","ERROR, A1"},
			{"log4j.appender.A1","org.apache.log4j.ConsoleAppender"},
			{"log4j.appender.A1.layout", "org.apache.log4j.PatternLayout"},
			{"log4j.appender.A1.layout.ConversionPattern", "[%t] %-5p %c %x - %m%n"},
	};
	
	private static ThreadPoolExecutor solrThreadPool = null;
	private static BlockingQueue<Runnable> solrWorkQueue = null;
	@Getter private static SCPSecurityFilter securityFilter = new SCPSecurityFilter();
	
	
	public static ThreadPoolExecutor getSolrThreadPool() {
		return solrThreadPool;
	}

	public static BlockingQueue<Runnable> getSolrWorkQueue() {
		return solrWorkQueue;
	}

	public static void main(String[] args) throws IOException {
		
		//log4j properties from Main properties file
		PropertyConfigurator.configure(getProperties());
		
		// setup solr workers
		
		int maximumPoolSize = getIntegerProperty("solrThreadsMaxPoolSize");
		int corePoolSize = getIntegerProperty("solrThreadsCorePoolSize");
		int keepAliveTime = getIntegerProperty("solrThreadsKeepAliveMins");

		solrWorkQueue = new ArrayBlockingQueue<Runnable>(maximumPoolSize) {
			private static final long serialVersionUID = 1L;};
		solrThreadPool = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.MINUTES, solrWorkQueue);

		// setup security filter
		
		String filterFilename = getStringProperty("SecurityFilterFile");
		try {
			securityFilter = SCPSecFilterXMLParser.parseFile(filterFilename);
		} catch (XMLStreamException e1) {
			log.error("Unable to read security filter file: {}", e1.getMessage());
			// it is fatal, if security cannot be enforced.
			System.exit(-1);
		}
		
		// Web Server invocation
		
		HttpServer httpServer = null;
		HttpContext httpContext=null;
		
		try {
			httpServer = HttpServer.create(new InetSocketAddress(getStringProperty("ListenHost"),getIntegerProperty("ListenPort")), 0);
			httpContext=httpServer.createContext("/solr",new SCPHttpHandler());
			httpContext.getFilters().add(new ParameterFilter());
			httpServer.setExecutor(Executors.newCachedThreadPool());
			httpServer.start();
			
		} catch (IOException e){
			log.error("Unable to listen on TCP port: {}", getIntegerProperty("ListenPort"));
			System.exit(-1);
		}
		
		try {
			Thread.currentThread().join();
		} catch (InterruptedException e) {
			log.info("Got interrupt");
		}
		log.info("SolrCloudProxy is shut down");
				
	}
	
	public static Properties getProperties() {
		if(properties==null) {
			loadProperties();
		}
		return properties;
	}
	
	public static String getStringProperty(String name) {
		return getProperties().getProperty(name);
	}
	
	public static Integer getIntegerProperty(String name) {
		return Integer.parseInt((String) getProperties().getProperty(name));
	}
	

	private static void loadProperties() {
		String propertiesFile="SolrCloudProxy.properties";
		if(System.getProperty("propertiesFile") != null) {
			propertiesFile=System.getProperty("propertiesFile");
		}
		properties=new Properties();
		try {
			FileInputStream in = new FileInputStream(propertiesFile);
			properties.load(in);
			in.close();
		} catch (IOException ie) {
			log.warn("Unable to load {}. Using default values.", propertiesFile);
		}
		setDefaultProperties();
	}


	private static void setDefaultProperties() {
		log.info("Using Properties:");
		for(String[] prop : defaultProperties ) {
			if(!properties.containsKey(prop[0])) {
				properties.setProperty(prop[0],prop[1]);
			}
			log.info("{}: {}",prop[0],properties.getProperty(prop[0]));
		}		
	}

}
