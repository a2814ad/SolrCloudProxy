/**
 * 
 */
package de.lrz.solrcloudproxy.mbeans;

/**
 * @author a2814ad
 *
 */
public interface StatisticsMBean {
	
	public int getNumQueries();
	public void increaseQueries();
	public float getElapsedTime();
	public void addElapsedTime(float delta);
	public float getMeanTime();
}
